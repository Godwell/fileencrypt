﻿using System;
using System.Collections.Generic;

namespace MCM.Models
{
    public partial class Upload
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string EncName { get; set; }
        public string ConvFile { get; set; }
    }
}
