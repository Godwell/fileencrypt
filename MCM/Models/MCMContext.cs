﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MCM.Models
{
    public partial class MCMContext : DbContext
    {
        public MCMContext()
        {
        }

        public MCMContext(DbContextOptions<MCMContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Upload> Upload { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-D505JLK\\SWIZZ;Initial Catalog=MCM;Integrated Security=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Upload>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ConvFile).HasColumnType("text");

                entity.Property(e => e.EncName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
