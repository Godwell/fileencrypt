﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml.Linq;
using MCM.Models;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;

namespace Common.Classes
{
    public class Common
    {
        public string RandomOTP()
        {
            // declare array string to generate random string with combination of small,capital letters and numbers
            char[] charArr = "0123456789".ToCharArray();

            string strrandom = string.Empty;
            Random objran = new Random();
            int noofcharacters = Convert.ToInt32(4);
            for (int i = 0; i < noofcharacters; i++)
            {
                //It will not allow Repetation of Characters
                int pos = objran.Next(1, charArr.Length);
                if (!strrandom.Contains(charArr.GetValue(pos).ToString()))
                    strrandom += charArr.GetValue(pos);
                else
                    i--;
            }
            return strrandom;
        }
        public static string Encrypt(byte[] clearText, string encriptionName)
        {
            string ctr = "";
            string EncryptionKey = encriptionName;
            //byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearText, 0, clearText.Length);
                        cs.Close();
                    }
                    ctr = Convert.ToBase64String(ms.ToArray());
                }
            }
            return ctr;
        }
        public static byte[] Decrypt(string cipherText, string encriptionName)
        {
            byte[] xyz;
            string EncryptionKey = encriptionName;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes   encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    xyz = ms.ToArray();
                }
            }
            return xyz;
        }
        public static void WriteAllBytes(string path, byte[] bytes)
        {
            

            FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Close();
            //File.SetAttributes(path, FileAttributes.Normal);
            //File.WriteAllBytes(path, bytes);
        }
        private static bool IsPrivateIP(string ipAddress)
        {

            //Private IPs {24-bit block: 10.0.0.0 - 10.255.255.255; 20-bit block: 172.16.0.0 - 172.31.255.255; 16-bit block: 192.168.0.0 - 192.168.255.255}
            //Link-local IPs {169.254.0.0 through 169.254.255.255}
            var ip = IPAddress.Parse(ipAddress);
            var octets = ip.GetAddressBytes();

            var is24Bit = octets[0] == 10;
            if (is24Bit) return true;

            var is20Bit = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20Bit) return true;

            var is16Bit = octets[0] == 192 && octets[1] == 168;
            if (is16Bit) return true;

            var isLinkLocal = octets[0] == 169 && octets[1] == 254;
            return isLinkLocal;

        }
        public static string GetMd5Sum(string str)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(str);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();

        }

        //hashing: SHA1
        public static string GetSHA1(string s_string)
        {
            string s_return = "";

            SHA1 sha = new SHA1Managed();
            ASCIIEncoding ae = new ASCIIEncoding();
            byte[] data = ae.GetBytes(s_string);
            byte[] digest = sha.ComputeHash(data);

            //return as hex
            StringBuilder s = new StringBuilder();
            int length = digest.Length;
            for (int n = 0; n < length; n++)
            {
                s.Append(String.Format("{0,2:X}", digest[n]).Replace(" ", "0"));
            }

            s_return = s.ToString();

            return s_return;

        }

        //encode BASE64
        //decode BASE64
        public static string ReturnEcodedString(string s_encodingmethod, string s_input)
        {
            string s_return = "";

            switch (s_encodingmethod.ToLower())
            {
                case "base64":
                    //decode base 64 string
                    var _bytes = Encoding.UTF8.GetBytes(s_input);
                    s_return = Convert.ToBase64String(_bytes);

                    break;
            }

            return s_return;
        }

        //decode BASE64
        public static string ReturnDecodedString(string s_encodingmethod, string s_input)
        {
            string s_return = "";

            switch (s_encodingmethod.ToLower())
            {
                case "base64":
                    //decode base 64 string
                    byte[] data = Convert.FromBase64String(s_input);
                    s_return = Encoding.UTF8.GetString(data);
                    break;
            }

            return s_return;
        }
        

        public class Constants
        {
            //*** THESE KEYS HAS BEEN INCLUDED IN THE CODE FOR SECURITY REASONS
            //public const string s_OGONE_C_KEY = "The*sun*shines*@eva1INDEV"; //*** Ogone Secret Key for SHA_IN
            //public const string s_OGONE_C_KEY_OUT = "The*sun*shines*@eva1OUTDEV"; //*** Ogone Secret Key for SHA_OUT
            //prod
            public const string s_OGONE_C_KEY = "PRDSunshine.2014"; //*** Ogone Secret Key for SHA_IN
            public const string s_OGONE_C_KEY_OUT = "PRDSunshine.2014"; //*** Ogone Secret Key for SHA_OUT

            public const string s_OGONE_C_APIUSER = "APIUser1"; //*** Ogone API Username
            public const string s_OGONE_C_APIKEY = ""; //*** Ogone API Password

            public enum InvoiceStatus { Paid, Pending, Due, Overdue, PaymentProcessing };
            public enum PredefinedCompanyDataFields { Email, Website, Phone, Mobile, Fax };

            public enum BannerSections
            {
                Homepage_BlogSection, Homepage_CompanyNewsSection_1, Homepage_CompanyNewsSection_2, Homepage_PressReleaseSection, Homepage_JobSearchSection,
                Blogpage_TaxBlogListingsPage, Blogpage_3rdpartyBlogListingsPage, Blogpage_BlogPost, Blogpage_Annoucement,
                AccountHolder_Dashboardpage, AccountHolder_Subscriptionpage, AccountHolder_Invoicespage, AccountHolder_Corporatedatapage,
                SearchResults_1, SearchResults_2, SearchResults_3
            }

        }
    }
}