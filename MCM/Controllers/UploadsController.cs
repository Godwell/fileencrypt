﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MCM.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;

namespace MCM.Controllers
{
    public class UploadsController : Controller
    {
        private readonly MCMContext _context;
        private readonly IHostingEnvironment environment;


        public UploadsController(MCMContext context, IHostingEnvironment environment)
        {
            _context = context;
            this.environment = environment;
        }


        // GET: Uploads
        public async Task<IActionResult> Index()
        {
            return View(await _context.Upload.ToListAsync());
        }

        // GET: Uploads/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Uploads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Upload upload, IFormFile file)
        {
            
                //string enc = upload.EncName;
                try
                {
                    byte[] fileArray;
                    using (var stream = file.OpenReadStream())
                    using (var memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);
                        fileArray = memoryStream.ToArray();
                    }

                    string encryptedFile = Common.Classes.Common.Encrypt(fileArray, upload.EncName);

                    string c = Path.Combine(environment.WebRootPath, "Uploads");

                    var fName = file.FileName.Split('.');

                    string fileName = fName[0] + ".txt";


                    using (StreamWriter outputFile = new StreamWriter(Path.Combine(c, fileName)))
                    {
                        outputFile.WriteLine(encryptedFile);
                    }
                    upload.FileName = fileName;
                    upload.ConvFile = c;
                    _context.Add(upload);
                    await _context.SaveChangesAsync();

                }
                catch (Exception ex)
                {

                }
            
            TempData["shortMsg"] = "File has Been Uploaded Successfully";
            return RedirectToAction("Index");
        }
        public IActionResult DownloadFile(long id)
        {
            return View();
        }
        [HttpPost]
        public IActionResult DownloadFile(Upload upload)
        {
            try
            {
                string conv = "";
                byte[] convFile;
                var uploads = _context.Upload.Find(upload.Id);

                if (upload.EncName != uploads.EncName)
                {
                    TempData["shortMsg"] = "Invalid Encryption Name";
                    return RedirectToAction("DownloadFile");
                }
                else
                {
                    string path = uploads.ConvFile + "\\" + uploads.FileName;

                    FileStream fileStream = new FileStream(path, FileMode.Open);

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        conv = reader.ReadLine();
                    }

                    convFile = Common.Classes.Common.Decrypt(conv, upload.EncName);
                    return File(convFile, System.Net.Mime.MediaTypeNames.Application.Octet);
                }
            }
            catch (Exception ex)
            {

            }

            return View();

        }
        private bool UploadExists(long id)
        {
            return _context.Upload.Any(e => e.Id == id);
        }
    }
}
